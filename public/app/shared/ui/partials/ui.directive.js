angular.module('webP.UI')
    .directive('uiHeader',
    function () {
        return {
            restrict: 'E',
            templateUrl: 'app/shared/ui/partials/header.html'
        }
    });
