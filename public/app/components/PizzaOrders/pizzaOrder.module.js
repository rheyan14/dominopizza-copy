angular.module('DominoPizza.List', [])
.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('pizzas.list',{
                url: '',
                templateUrl: 'app/components/PizzaOrders/partials/pizzaList.html'
            })
    }
]);
